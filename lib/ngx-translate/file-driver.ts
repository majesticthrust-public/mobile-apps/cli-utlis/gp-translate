import path from "path";
import fs from "fs-extra";

/**
 * ngx-translate app translation. Equivalent to `any` type.
 */
export type INgxTranslation = any;

export class NgxTranslateFileDriver {
  /**
   * @param baseDir directory with app translations
   */
  constructor(private baseDir: string) {}

  /**
   * Checks whether translation for a given language exists.
   * @param lang language ISO code
   */
  public translationExists(lang: string) {
    const p = this.getFilePath(lang);
    return fs.pathExists(p);
  }

  /**
   * Returns translation for a given language. Throws if it doesn't exist.
   * @param lang language ISO code
   */
  public async getTranslation(lang: string): Promise<INgxTranslation> {
    const p = this.getFilePath(lang);
    return fs.readJSON(p) as Promise<INgxTranslation>;
  }

  /**
   * Writes translation for a given language.
   * @param lang language ISO code
   * @param translation app data translated to language `lang`
   */
  public async setTranslation(lang: string, translation: INgxTranslation) {
    const p = this.getFilePath(lang);
    return fs.outputJSON(p, translation, { spaces: 2 });
  }

  /**
   * Returns path to the translation file for a given language.
   * @param lang language ISO code
   */
  private getFilePath(lang: string) {
    return path.join(this.baseDir, lang + ".json");
  }
}
