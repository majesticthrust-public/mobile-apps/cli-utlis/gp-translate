import _ from "lodash";
import { TranslateFunction, getTranslator } from "../util/translator";
import { NgxTranslateFileDriver, INgxTranslation } from "./file-driver";
import languages from "../util/languages";

export class NgxTranslateTranslator {
  private translator: TranslateFunction;
  private translations: NgxTranslateFileDriver;

  /**
   * Set of language ISO codes
   */
  private languages: Set<string>;

  /**
   * @param baseDir directory with app translations
   */
  constructor(baseDir: string, languageWhitelist?: string[]) {
    this.translator = getTranslator();
    this.translations = new NgxTranslateFileDriver(baseDir);
    this.languages = new Set(languages.map((l) => l.translateCode));

    // filter whitelisted languages
    if (languageWhitelist != null) {
      const whitelistedLanguages = new Set(languageWhitelist);
      this.languages = new Set(
        [...this.languages].filter((l) => whitelistedLanguages.has(l))
      );
    }
  }

  /**
   * Creates new translations and fills the incomplete ones based on the source translation.
   * @param sourceLang language ISO code for the source translation
   * @param progressCallback called every time a language is done processing
   */
  public async translate(
    sourceLang: string,
    progressCallback?: (language: string) => void
  ) {
    if (!this.translations.translationExists(sourceLang)) {
      throw new Error(
        `Translation for source language "${sourceLang}" doesn't exist!`
      );
    }

    const sourceTranslation = await this.translations.getTranslation(
      sourceLang
    );
    const otherLanguages = new Set(this.languages);
    otherLanguages.delete(sourceLang);

    for (const lang of otherLanguages) {
      let langTranslation: INgxTranslation;

      if (await this.translations.translationExists(lang)) {
        // check the difference and fill the missing translations
        langTranslation = await this.translations.getTranslation(lang);
        langTranslation = await this.diffTranslateLanguage(
          langTranslation,
          sourceTranslation,
          sourceLang,
          lang
        );
      } else {
        // create new translation
        langTranslation = await this.translateLanguage(
          sourceTranslation,
          sourceLang,
          lang
        );
      }

      this.translations.setTranslation(lang, langTranslation);

      if (progressCallback) {
        progressCallback(lang);
      }
    }
  }

  /**
   * Translates given ngx-translate text object recursively. Does not modify the input `translation` object.
   * @param translation text that needs translating
   * @param from language ISO code
   * @param to language ISO code
   */
  private async translateLanguage(
    translation: INgxTranslation,
    from: string,
    to: string
  ): Promise<INgxTranslation> {
    const newTranslation = {};
    const promises: Array<Promise<void>> = [];

    this.walkNgxTranslationTree(async (value, path) => {
      const p = new Promise(async (resolve, reject) => {
        const result = await this.translator(value, { from, to });
        _.set(newTranslation, path, result.text);
        resolve();
      });
      promises.push(p as Promise<void>);
    }, translation);

    await Promise.all(promises);
    return newTranslation;
  }

  /**
   * Translates the keys that exist in `sourceTranslation` but are missing in `translation`
   * and writes them to `translation`.
   * Returns a fully translated object.
   * @param translation text that needs translating
   * @param sourceTranslation translation to compare with
   * @param from language ISO code
   * @param to language ISO code
   */
  private async diffTranslateLanguage(
    translation: INgxTranslation,
    sourceTranslation: INgxTranslation,
    from: string,
    to: string
  ): Promise<INgxTranslation> {
    const newTranslation = {};
    const promises: Array<Promise<void>> = [];

    this.walkNgxTranslationTree(async (value, path) => {
      const p = new Promise(async (resolve, reject) => {
        const existing = _.get(translation, path, null);
        if (existing === null) {
          // if translation is missing, translate and assign
          const result = await this.translator(value, { from, to });
          _.set(newTranslation, path, result.text);
        } else {
          // if translation exists, just copy it over
          _.set(newTranslation, path, existing);
        }
        resolve();
      });
      promises.push(p as Promise<void>);
    }, sourceTranslation);

    await Promise.all(promises);
    return newTranslation;
  }

  /**
   * Walk the NgxTranslation tree recursively. Call the `leafCallback` on every leaf.
   * @param leafCallback callback to call on every leaf; leaf value as well as nested path are passed as arguments
   * @param node current node
   * @param path current path; `undefined` if root
   */
  private walkNgxTranslationTree(
    leafCallback: (value: string, path: Array<string | number>) => void,
    node: INgxTranslation,
    path?: Array<string | number>
  ) {
    if (path === undefined) {
      path = [];
    }

    if (typeof node === "string") {
      // string is a leaf, call the leafCallback
      leafCallback(node, path);
    } else if (typeof node === "object") {
      for (const key of Object.keys(node)) {
        this.walkNgxTranslationTree(leafCallback, node[key], [...path, key]);
      }
    } else if (Array.isArray(node)) {
      node.forEach((value, i) =>
        this.walkNgxTranslationTree(leafCallback, node[i], [...path, i])
      );
    } else {
      throw new Error(`Unsupported type: "${typeof node}"!`);
    }
  }
}
