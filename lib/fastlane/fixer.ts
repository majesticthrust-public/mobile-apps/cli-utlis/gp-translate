import { FastlaneAndroidMetadataAccess, IFastlaneMetadata } from "./metadata";
import { FastlaneMetadataValidator } from "./validator";
import { ILanguage } from "../util/languages";

/**
 * Fixes metadata for Google Play. Uses fastlane metadata directory structure.
 */
export class FastlaneMetadataFixer {
  private metadataAccess: FastlaneAndroidMetadataAccess;

  /**
   * @param baseDir base directory of metadata
   */
  constructor(baseDir: string) {
    this.metadataAccess = new FastlaneAndroidMetadataAccess(baseDir);
  }

  /**
   * Fix metadata for given languages
   * @param languagesToFix which languages need a fix
   * @param method
   */
  public async fix(languagesToFix: ILanguage[], method: string) {
    // TODO unify methods: apply trim, punctuation, trimword sequentially until fixed
    let fixer;
    switch (method) {
      case "trim":
        fixer = this._fix_method_trim;
        break;

      case "punctuation":
        fixer = this._fix_method_punctuation;
        break;

      case "trimword":
        fixer = this._fix_method_trimword;
        break;

      default:
        throw new Error(`Invalid fix method "${method}"`);
    }

    for (const lang of languagesToFix) {
      let metadata: IFastlaneMetadata;
      let isFixed = false;

      try {
        metadata = await this.metadataAccess.getMetadata(lang.code);
      } catch (e) {
        // metadata not found
        // console.log(`${lang.code} not found`);
        continue;
      }

      if (metadata.title.length > FastlaneMetadataValidator.titleMaxLength) {
        console.log(`Language ${lang.code}; fixing title`);
        metadata.title = fixer(
          metadata.title,
          FastlaneMetadataValidator.titleMaxLength,
        );
        isFixed = true;
      }

      if (
        metadata.shortDescription.length >
        FastlaneMetadataValidator.shortDescriptionMaxLength
      ) {
        console.log(`Language ${lang.code}; fixing short description`);
        metadata.shortDescription = fixer(
          metadata.shortDescription,
          FastlaneMetadataValidator.shortDescriptionMaxLength,
        );
        isFixed = true;
      }

      if (
        metadata.fullDescription.length >
        FastlaneMetadataValidator.fullDescriptionMaxLength
      ) {
        console.log(`Language ${lang.code}; fixing full description`);
        metadata.fullDescription = fixer(
          metadata.fullDescription,
          FastlaneMetadataValidator.fullDescriptionMaxLength,
        );
        isFixed = true;
      }

      if (isFixed) {
        this.metadataAccess.setMetadata(lang.code, metadata);
      }
    }
  }

  /**
   * Trim excess characters from the end
   * @param str
   * @param desiredLength
   */
  private _fix_method_trim(str: string, desiredLength: number) {
    return str.substr(0, desiredLength);
  }

  /**
   * Strip all punctuation from string
   * @param str
   * @param desiredLength
   */
  private _fix_method_punctuation(str: string, desiredLength: number) {
    return str
      .replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, "") // punctuation
      .replace(/\s{2,}/g, " "); // extra spaces
  }

  /**
   * Remove words from the end until desired length
   * @param str
   * @param desiredLength
   */
  private _fix_method_trimword(str: string, desiredLength: number) {
    while (str.length > desiredLength) {
      // tslint:disable-next-line:variable-name
      const space_i = str.lastIndexOf(" ");
      str = str.substring(0, space_i);
    }

    return str;
  }
}
