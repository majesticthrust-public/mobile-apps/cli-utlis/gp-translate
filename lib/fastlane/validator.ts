import { FastlaneAndroidMetadataAccess, IFastlaneMetadata } from "./metadata";
import languages from "../util/languages";

/**
 * Validates metadata for Google Play. Uses fastlane metadata directory structure.
 */
export class FastlaneMetadataValidator {
  private metadataAccess: FastlaneAndroidMetadataAccess;
  static get titleMaxLength() {
    return 50;
  }
  static get shortDescriptionMaxLength() {
    return 80;
  }
  static get fullDescriptionMaxLength() {
    return 4000;
  }

  /**
   * @param baseDir base directory of metadata
   */
  constructor(baseDir: string) {
    this.metadataAccess = new FastlaneAndroidMetadataAccess(baseDir);
  }

  public async validate() {
    const invalidLanguages = [];

    for (const lang of languages) {
      let metadata: IFastlaneMetadata;
      let isInvalid = false;

      try {
        metadata = await this.metadataAccess.getMetadata(lang.code);
      } catch (e) {
        // metadata not found
        // console.log(`${lang.code} not found`);
        continue;
      }

      const { title, shortDescription, fullDescription } = metadata;

      if (title.length > FastlaneMetadataValidator.titleMaxLength) {
        console.log(
          `Language ${lang.code}; title is too long (is ${title.length} chars, max is ${FastlaneMetadataValidator.titleMaxLength})`,
        );
        isInvalid = true;
      }

      if (
        shortDescription.length >
        FastlaneMetadataValidator.shortDescriptionMaxLength
      ) {
        console.log(
          `Language ${lang.code}; short description is too long (is ${shortDescription.length} chars, max is ${FastlaneMetadataValidator.shortDescriptionMaxLength})`,
        );
        isInvalid = true;
      }

      if (
        fullDescription.length >
        FastlaneMetadataValidator.fullDescriptionMaxLength
      ) {
        console.log(
          `Language ${lang.code}; full description is too long (is ${fullDescription.length} chars, max is ${FastlaneMetadataValidator.fullDescriptionMaxLength})`,
        );
        isInvalid = true;
      }

      if (isInvalid) {
        invalidLanguages.push(lang);
      }
    }

    return invalidLanguages;
  }
}
