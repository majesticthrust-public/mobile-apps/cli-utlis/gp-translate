import ProgressBar from "progress";
import { FastlaneAndroidMetadataAccess, IFastlaneMetadata } from "./metadata";

import languages, { ILanguage } from "../util/languages";
import { getTranslator, TranslateFunction } from "../util/translator";

export class FastlaneTranslator {
  private metadataAccess: FastlaneAndroidMetadataAccess;
  private translator: TranslateFunction;

  /**
   * @param baseDir base directory to create `fastlane supply` structure in
   */
  constructor(baseDir: string) {
    this.metadataAccess = new FastlaneAndroidMetadataAccess(baseDir);
    this.translator = getTranslator();
  }

  /**
   * Creates metadata translated from source language
   * @param sourceLangCode source language code to translate from
   */
  public async translate(sourceLangCode: string) {
    const sourceMetadata = await this.metadataAccess.getMetadata(sourceLangCode);
    const sourceLang = languages.find((lang) => lang.code === sourceLangCode);

    if (sourceLang === undefined) {
      throw new Error(`Invalid source lang code: "${sourceLangCode}"!`);
    }

    const progress = new ProgressBar(
      "[:current/:total] [:bar] elapsed :elapseds | language :languageName",
      {
        total: languages.length,
        width: 30,
      },
    );

    for (const lang of languages) {
      const result = await this.translateMetadata(
        sourceLang,
        lang,
        sourceMetadata,
      );
      this.metadataAccess.setMetadata(lang.code, result);

      progress.tick({
        languageName: lang.name,
      });
    }
  }

  /**
   * Translates text metadata
   * @param fromLang source language
   * @param toLang target language
   * @param metadata texts to put into new dir
   */
  private async translateMetadata(
    fromLang: ILanguage,
    toLang: ILanguage,
    metadata: IFastlaneMetadata,
  ) {
    if (fromLang.translateCode === toLang.translateCode) {
      return metadata;
    }

    const translateParams = {
      from: fromLang.translateCode,
      to: toLang.translateCode,
      // client: "gtx"
    };

    return {
      title: (await this.translator(metadata.title, translateParams)).text,
      shortDescription: (await this.translator(
        metadata.shortDescription,
        translateParams,
      )).text,
      fullDescription: (await this.translator(
        metadata.fullDescription,
        translateParams,
      )).text,
    };
  }
}
