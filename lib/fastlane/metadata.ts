import path from "path";
import fs from "fs-extra";

/**
 * Fastlane app text metadata
 */
export interface IFastlaneMetadata {
  /**
   * App title
   */
  title: string;

  /**
   * App short description
   */
  shortDescription: string;

  /**
   * App full description
   */
  fullDescription: string;
}

/**
 * Provides read/write access to fastlane android metadata in async api way.
 */
export class FastlaneAndroidMetadataAccess {
  private baseDir: string;

  /**
   * @param baseDir base directory of fastlane metadata for android platform
   */
  constructor(baseDir: string) {
    this.baseDir = baseDir;
  }

  /**
   * Try to get metadata for a given language code.
   * Rejects if any of [title, shortDescription, longDescription] not found.
   * @param langCode language code (e.g. en-US)
   */
  public async getMetadata(langCode: string): Promise<IFastlaneMetadata> {
    this.validateLangCode(langCode);

    const langMetadataDir = path.join(this.baseDir, langCode);

    const title = (await fs.readFile(
      path.join(langMetadataDir, "title.txt"),
    )).toString();

    const shortDescription = (await fs.readFile(
      path.join(langMetadataDir, "short_description.txt"),
    )).toString();

    const fullDescription = (await fs.readFile(
      path.join(langMetadataDir, "full_description.txt"),
    )).toString();

    return {
      title,
      shortDescription,
      fullDescription,
    };
  }

  /**
   * Sets/creates metadata
   * @param langCode ISO language code as used by fastlane
   * @param metadata metadata to set
   */
  public async setMetadata(
    langCode: string,
    metadata: IFastlaneMetadata,
  ): Promise<void> {
    this.validateLangCode(langCode);

    const langMetadataDir = path.join(this.baseDir, langCode);
    await fs.ensureDir(langMetadataDir);

    await Promise.all([
      fs.outputFile(path.join(langMetadataDir, "title.txt"), metadata.title),

      fs.outputFile(
        path.join(langMetadataDir, "short_description.txt"),
        metadata.shortDescription,
      ),

      fs.outputFile(
        path.join(langMetadataDir, "full_description.txt"),
        metadata.fullDescription,
      ),
    ]);
  }

  /**
   * Throws error if language code is invalid
   * @param langCode ISO language code (e.g. en-US)
   */
  private validateLangCode(langCode: string) {
    // TODO check langCode validity

    // if (!isLangCodeValid(langCode)) {
    //   throw new Error(`Invalid language code "${langCode}"!`);
    // }

    return true;
  }
}
