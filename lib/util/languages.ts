export interface ILanguage {
  /**
   * Full language (and region) name
   */
  name: string;

  /**
   * ISO language & region code
   */
  code: string;

  /**
   * ISO code as used by google-translate-api
   */
  translateCode: string;
}

export default [
  {
    name: "English (United States)",
    code: "en-US",
    translateCode: "en",
  },
  {
    name: "Afrikaans",
    code: "af",
    translateCode: "af",
  },
  {
    name: "Amharic",
    code: "am",
    translateCode: "am",
  },
  {
    name: "Arabic",
    code: "ar",
    translateCode: "ar",
  },
  {
    name: "Armenian",
    code: "hy-AM",
    translateCode: "hy",
  },
  {
    name: "Azerbaijani",
    code: "az-AZ",
    translateCode: "az",
  },
  {
    name: "Bangla",
    code: "bn-BD",
    translateCode: "bn",
  },
  {
    name: "Basque",
    code: "eu-ES",
    translateCode: "eu",
  },
  {
    name: "Belarusian",
    code: "be",
    translateCode: "be",
  },
  {
    name: "Bulgarian",
    code: "bg",
    translateCode: "bg",
  },
  {
    name: "Burmese",
    code: "my-MM",
    translateCode: "my",
  },
  {
    name: "Catalan",
    code: "ca",
    translateCode: "ca",
  },
  {
    name: "Chinese (Hong Kong)",
    code: "zh-HK",
    translateCode: "zh-CN",
  },
  {
    name: "Chinese (Simplified)",
    code: "zh-CN",
    translateCode: "zh-CN",
  },
  {
    name: "Chinese (Traditional)",
    code: "zh-TW",
    translateCode: "zh-TW",
  },
  {
    name: "Croatian",
    code: "hr",
    translateCode: "hr",
  },
  {
    name: "Czech",
    code: "cs-CZ",
    translateCode: "cs",
  },
  {
    name: "Danish",
    code: "da-DK",
    translateCode: "da",
  },
  {
    name: "Dutch",
    code: "nl-NL",
    translateCode: "nl",
  },
  {
    name: "English",
    code: "en-IN",
    translateCode: "en",
  },
  {
    name: "English",
    code: "en-SG",
    translateCode: "en",
  },
  {
    name: "English",
    code: "en-ZA",
    translateCode: "en",
  },
  {
    name: "English (Australia)",
    code: "en-AU",
    translateCode: "en",
  },
  {
    name: "English (Canada)",
    code: "en-CA",
    translateCode: "en",
  },
  {
    name: "English (United Kingdom)",
    code: "en-GB",
    translateCode: "en",
  },
  {
    name: "Estonian",
    code: "et",
    translateCode: "et",
  },
  {
    name: "Filipino",
    code: "fil",
    translateCode: "tl",
  },
  {
    name: "Finnish",
    code: "fi-FI",
    translateCode: "fi",
  },
  {
    name: "French",
    code: "fr-FR",
    translateCode: "fr",
  },
  {
    name: "French (Canada)",
    code: "fr-CA",
    translateCode: "fr",
  },
  {
    name: "Galician",
    code: "gl-ES",
    translateCode: "gl",
  },
  {
    name: "Georgian",
    code: "ka-GE",
    translateCode: "ka",
  },
  {
    name: "German",
    code: "de-DE",
    translateCode: "de",
  },
  {
    name: "Greek",
    code: "el-GR",
    translateCode: "el",
  },
  {
    name: "Hebrew",
    code: "iw-IL",
    translateCode: "iw",
  },
  {
    name: "Hindi",
    code: "hi-IN",
    translateCode: "hi",
  },
  {
    name: "Hungarian",
    code: "hu-HU",
    translateCode: "hu",
  },
  {
    name: "Icelandic",
    code: "is-IS",
    translateCode: "is",
  },
  {
    name: "Indonesian",
    code: "id",
    translateCode: "id",
  },
  {
    name: "Italian",
    code: "it-IT",
    translateCode: "it",
  },
  {
    name: "Japanese",
    code: "ja-JP",
    translateCode: "ja",
  },
  {
    name: "Kannada",
    code: "kn-IN",
    translateCode: "kn",
  },
  {
    name: "Kazakh",
    code: "kk",
    translateCode: "kk",
  },
  {
    name: "Khmer",
    code: "km-KH",
    translateCode: "km",
  },
  {
    name: "Korean (South Korea)",
    code: "ko-KR",
    translateCode: "ko",
  },
  {
    name: "Kyrgyz",
    code: "ky-KG",
    translateCode: "ky",
  },
  {
    name: "Lao",
    code: "lo-LA",
    translateCode: "lo",
  },
  {
    name: "Latvian",
    code: "lv",
    translateCode: "lv",
  },
  {
    name: "Lithuanian",
    code: "lt",
    translateCode: "lt",
  },
  {
    name: "Macedonian",
    code: "mk-MK",
    translateCode: "mk",
  },
  {
    name: "Malay",
    code: "ms",
    translateCode: "ms",
  },
  {
    name: "Malay (Malaysia)",
    code: "ms-MY",
    translateCode: "ms",
  },
  {
    name: "Malayalam",
    code: "ml-IN",
    translateCode: "ml",
  },
  {
    name: "Marathi",
    code: "mr-IN",
    translateCode: "mr",
  },
  {
    name: "Mongolian",
    code: "mn-MN",
    translateCode: "mn",
  },
  {
    name: "Nepali",
    code: "ne-NP",
    translateCode: "ne",
  },
  {
    name: "Norwegian",
    code: "no-NO",
    translateCode: "no",
  },
  {
    name: "Persian",
    code: "fa",
    translateCode: "fa",
  },
  {
    name: "Persian",
    code: "fa-AE",
    translateCode: "fa",
  },
  {
    name: "Persian",
    code: "fa-AF",
    translateCode: "fa",
  },
  {
    name: "Persian",
    code: "fa-IR",
    translateCode: "fa",
  },
  {
    name: "Polish",
    code: "pl-PL",
    translateCode: "pl",
  },
  {
    name: "Portuguese (Brazil)",
    code: "pt-BR",
    translateCode: "pt",
  },
  {
    name: "Portuguese (Portugal)",
    code: "pt-PT",
    translateCode: "pt",
  },
  {
    name: "Punjabi",
    code: "pa",
    translateCode: "pa",
  },
  {
    name: "Romanian",
    code: "ro",
    translateCode: "ro",
  },
  // not supported, not needed
  // {
  //   name: "Romansh",
  //   code: "rm",
  //   translateCode: "rm"
  // },
  {
    name: "Russian",
    code: "ru-RU",
    translateCode: "ru",
  },
  {
    name: "Serbian",
    code: "sr",
    translateCode: "sr",
  },
  {
    name: "Sinhala",
    code: "si-LK",
    translateCode: "si",
  },
  {
    name: "Slovak",
    code: "sk",
    translateCode: "sk",
  },
  {
    name: "Slovenian",
    code: "sl",
    translateCode: "sl",
  },
  {
    name: "Spanish (Latin America)",
    code: "es-419",
    translateCode: "es",
  },
  {
    name: "Spanish (Spain)",
    code: "es-ES",
    translateCode: "es",
  },
  {
    name: "Spanish (United States)",
    code: "es-US",
    translateCode: "es",
  },
  {
    name: "Swahili",
    code: "sw",
    translateCode: "sw",
  },
  {
    name: "Swedish",
    code: "sv-SE",
    translateCode: "sv",
  },
  {
    name: "Tamil",
    code: "ta-IN",
    translateCode: "ta",
  },
  {
    name: "Telugu",
    code: "te-IN",
    translateCode: "te",
  },
  {
    name: "Thai",
    code: "th",
    translateCode: "th",
  },
  {
    name: "Turkish",
    code: "tr-TR",
    translateCode: "tr",
  },
  {
    name: "Ukrainian",
    code: "uk",
    translateCode: "uk",
  },
  {
    name: "Vietnamese",
    code: "vi",
    translateCode: "vi",
  },
  {
    name: "Zulu",
    code: "zu",
    translateCode: "zu",
  },
] as ILanguage[];
