import memoize from "mem";
import Bottleneck from "bottleneck";

// https://github.com/vitalets/google-translate-api

export interface ITranslateOptions {
  from?: "auto" | string;
  to?: string;
  raw?: boolean;
  client?: "t" | "gtx";
  tld?: string;
}

/**
 * https://github.com/sindresorhus/got#options
 */
export type ITranslateGotOptions = object;

export interface ITranslateResult {
  text: string;
  from: {
    language: {
      didYouMean: boolean;
      iso: string;
    };
    text: {
      autoCorrected: boolean;
      value: string;
      didYouMean: boolean;
    };
    raw: string;
  };
}

export type TranslateFunction = (
  text: string,
  options?: ITranslateOptions,
  gotOptions?: ITranslateGotOptions,
) => Promise<ITranslateResult>;

// there are no type definitions, hence this
// tslint:disable-next-line:no-var-requires
const translate: TranslateFunction = require("@vitalets/google-translate-api");

// TODO dependency injection

/**
 * Module-cached function
 */
let translator: TranslateFunction | null = null;

/**
 * Initializes a translate function with caching and applied limits.
 * @param reqPerSecond max requests per second
 * @param reqConcurrent max amount of concurrent calls
 */
export function initTranslator(reqPerSecond: number, reqConcurrent: number) {
  const limiter = new Bottleneck({
    minTime: 1000 / reqPerSecond,
    maxConcurrent: reqConcurrent,
  });
  const throttled = limiter.wrap(translate);
  translator = memoize(throttled) as TranslateFunction;
}

export function getTranslator() {
  if (translator === null) {
    throw new Error("Translator is not initialized!");
  }

  return translator;
}
