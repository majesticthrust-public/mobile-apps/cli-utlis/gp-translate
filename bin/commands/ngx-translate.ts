import { Argv, Arguments } from "yargs";
import { NgxTranslateTranslator } from "../../lib/ngx-translate/translator";

/*
TODO add alternative way to handle translations:
Allow to specify a mapping of languages to files.
This mapping should serve both as a whitelist to restrict the files affected and as a labelling device to separate languages from file names.
*/

export const command = "ngx-translate";
export const description =
  "Translate app text for `ngx-translate`. Translates from the source language to all that are supported by Google Play. Detects and translates missing keys.";

export const builder = (yargs: Argv) =>
  yargs
    .option("dir", {
      type: "string",
      description: "Directory with app translation files.",
      requiresArg: true,
    })
    .demandOption("dir")
    .option("source-language", {
      type: "string",
      description:
        "Source language to take the text from. Essentially the name of the translation file (e.g. en.json)",
      requiresArg: true,
      default: "en",
    })
    .option("language-whitelist", {
      alias: "w",
      type: "array",
      description:
        "Specify which languages should the source language be translated to. The source language is included in this list automatically.",
      // requiresArg: true,
    });

export const handler = async (argv: Arguments) => {
  let languageWhitelist: string[] = [];
  if (argv.languageWhitelist != null) {
    languageWhitelist = argv["language-whitelist"] as string[];
    languageWhitelist.push(argv["source-language"] as string);
  }

  const translator = new NgxTranslateTranslator(
    argv.dir as string,
    languageWhitelist.length > 0 ? languageWhitelist : undefined
  );

  console.log("Translating...");
  await translator.translate(argv["source-language"] as string, console.log);
};
