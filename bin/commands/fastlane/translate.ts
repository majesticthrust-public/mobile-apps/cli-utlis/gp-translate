import { Argv, Arguments } from "yargs";
import { FastlaneTranslator } from "../../../lib/fastlane/translator";

export const command = "translate";
export const description = "Translate app metadata for fastlane.";

export const builder = (yargs: Argv) =>
  yargs
    .option("source-language", {
      alias: ["source", "s"],
      type: "string",
      description:
        "Source language to base all translations on. Essentially the name of a directory in `fastlane/metadata/android` folder.",
      requiresArg: true,
      default: "en-US",
    });
    // .option("client", {
    //   type: "string",
    //   description: "Google Translate client param",
    //   choices: ["gtx", "t"],
    //   default: "gtx",
    // });

export const handler = async (argv: Arguments) => {
  const translator = new FastlaneTranslator(argv.dir as string);
  translator.translate(argv["source-language"] as string);
};
