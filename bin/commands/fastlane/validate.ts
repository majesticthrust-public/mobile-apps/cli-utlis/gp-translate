import { Arguments, Argv } from "yargs";
import { FastlaneMetadataValidator } from "../../../lib/fastlane/validator";
import { FastlaneMetadataFixer } from "../../../lib/fastlane/fixer";

export const command = "validate";
export const description = `Validate app metadata for fastlane (and optionally fix it).
Checks the length of the title, short and long description for all languages.
Fixing the length involves removal of characters via one of multiple methods.`;

export const builder = (yargs: Argv) =>
  yargs.option("fix", {
    description: `Fix errors with specified method:
- trim: remove excess characters from the end of the string;
- punctuation: remove all punctuation from the string;
- trimword: remove words from the end of the string`,
    // TODO remove options, unify fixing methods
    choices: ["trim", "punctuation", "trimword"],
  });

export const handler = async (argv: Arguments) => {
  const validator = new FastlaneMetadataValidator(argv.dir as string);
  const fixLangs = await validator.validate();

  if (argv.fix) {
    const fixer = new FastlaneMetadataFixer(argv.dir as string);
    fixer.fix(fixLangs, argv.fix as string);
  }
};
