import { Argv } from "yargs";

export const command = "fastlane <command>";
export const description = "Fastlane metadata commands";

export const builder = (yargs: Argv) =>
  yargs
    .option("dir", {
      type: "string",
      default: ".",
      describe:
        "The metadata directory (typically located in `$PROJECT/fastlane/metadata/android)`",
      global: true,
      requiresArg: true,
    })
    .demandOption("dir")
    .commandDir("fastlane")
    .demandCommand();

// export const handler = () => {};
