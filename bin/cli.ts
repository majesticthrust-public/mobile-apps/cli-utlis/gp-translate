#!/usr/bin/env node

// import yargs from "yargs";

// const args = yargs
//   .option("throttle", {
//     description: "Limit amount of requests per second",
//     type: "number",
//     default: 5,
//     global: true,
//     requiresArg: true,
//   })
//   .commandDir("commands")
//   .demandCommand()
//   .help().argv;

import yargs from "yargs";
import { initTranslator } from "../lib/util/translator";

const yargsConfig = yargs.option("throttle", {
  description: "Limit amount of requests per second",
  type: "number",
  default: 1,
  global: true,
  requiresArg: true,
});

// Initialize stuff
// TODO change to dependency injection or something
const args = yargsConfig.argv;
initTranslator(args.throttle, 1);

// continue parsing
// tslint:disable-next-line:no-unused-expression
yargsConfig
  .commandDir("commands")
  .demandCommand()
  .help().argv;
